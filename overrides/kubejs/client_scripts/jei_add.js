// priority: 0

console.info('Add JEI Items!')

onEvent('jei.add.items', event => {
    // add items in JEI here
    // event.add('minecraft:cobblestone')

    //Minecraft
    event.add('minecraft:dragon_egg')

    // TConstruct
    event.add('tconstruct:crafting_station')
    event.add('tconstruct:tinker_station')
    event.add('tconstruct:part_builder')
    event.add('tconstruct:tinkers_anvil')
    event.add('tconstruct:scorched_anvil')

})
