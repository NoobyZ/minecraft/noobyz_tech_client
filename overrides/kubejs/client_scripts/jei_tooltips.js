// priority: 0

console.info('Write Tooltips for JEI Items!')

onEvent('item.tooltip', event => {

    // Cyclic
    event.add('cyclic:cask', [
        Text.yellow('Stores up to 8 buckets of any liquid.'),
        Text.yellow('Does not keep contents when broken.')
    ])

    /*
    // Refined Storage
    event.add([
        'refinedstorage:controller',
        'refinedstorage:grid',
        'refinedstorage:crafting_grid',
        'refinedstorage:pattern_grid',
        'refinedstorage:fluid_grid',
        'refinedstorage:network_receiver',
        'refinedstorage:relay',
        'refinedstorage:disk_manipulator',
        'refinedstorage:crafter',
        'refinedstorage:crafting_monitor',
        'refinedstorage:detector',
        'refinedstorage:network_transmitter',
        'refinedstorage:wireless_transmitter',
        'refinedstorage:crafter_manager',
        'refinedstorage:security_manager'
    ], [
        Text.yellow('Color can be changed through crafting,'),
        Text.yellow('or by right clicking it with dye.'),
    ])
    */

    // Industrialforegoing
    event.add('industrialforegoing:latex_bucket', [
        Text.yellow('Not equivalent to Thermal\'s Latex.'),
    ])

    // Thermal
    event.add('thermal:latex_bucket', [
        Text.yellow('Not equivalent to Industrial Foregoing\'s Latex.'),
    ])

    // Compressium
    event.add(/compressium:.+1/, [
        Text.yellow('9 Blocks'),
    ])
    event.add(/compressium:.+2/, [
        Text.yellow('81 Blocks'),
    ])
    event.add(/compressium:.+3/, [
        Text.yellow('729 Blocks'),
    ])
    event.add(/compressium:.+4/, [
        Text.yellow('6,561 Blocks'),
    ])
    event.add(/compressium:.+5/, [
        Text.yellow('59,049 Blocks'),
    ])
    event.add(/compressium:.+6/, [
        Text.yellow('531,441 Blocks'),
    ])
    event.add(/compressium:.+7/, [
        Text.yellow('4,782,969 Blocks'),
    ])
    event.add(/compressium:.+8/, [
        Text.yellow('43,046,721 Blocks'),
    ])
    event.add(/compressium:.+9/, [
        Text.yellow('387,420,489 Blocks'),
    ])

    // Pipez
    event.add('pipez:basic_upgrade', [
        Text.yellow('Provides:'),
        Text.yellow('Redstone modes')
    ])
    event.add('pipez:improved_upgrade', [
        Text.yellow('Provides:'),
        Text.yellow('Redstone modes'),
        Text.yellow('Distribution modes'),
    ])
    event.add([
        'pipez:advanced_upgrade',
        'pipez:ultimate_upgrade'
    ], [
        Text.yellow('Provides:'),
        Text.yellow('Redstone modes'),
        Text.yellow('Distribution modes'),
        Text.yellow('Filters'),
        Text.yellow('Filter modes')
    ])

    // TConstruct
    event.add([
        'tconstruct:part_builder',
        'tconstruct:tinker_station'
    ], [
        Text.yellow('Can be made out of any planks.'),
    ])
    event.add('tconstruct:crafting_station', [
        Text.yellow('Can be made out of any log.'),
    ])
    event.add([
        'tconstruct:tinkers_anvil',
        'tconstruct:scorched_anvil'
    ], [
        Text.yellow('Can be made out of any alloy block.'),
    ])

    // Immersive Engineering
    event.add('immersiveengineering:metal_press', [
        Text.yellow('1x Piston'),
        Text.yellow('2x Steel Scaffolding'),
        Text.yellow('1x Redstone Engineering Block'),
        Text.yellow('1x Heavy Engineering Block'),
        Text.yellow('2x Conveyor Belt')
    ])
    event.add('immersiveengineering:assembler', [
        Text.yellow('6x Steel Scaffolding'),
        Text.yellow('2x Redstone Engineering Block'),
        Text.yellow('2x Light Engineering Block'),
        Text.yellow('9x Iron Sheetmetal'),
        Text.yellow('6x Iron Sheetmetal Slab'),
        Text.yellow('2x Conveyor Belt')
    ])
    event.add('immersiveengineering:squeezer', [
        Text.yellow('1x Piston'),
        Text.yellow('6x Steel Scaffolding'),
        Text.yellow('2x Light Engineering Block'),
        Text.yellow('1x Redstone Engineering Block'),
        Text.yellow('3x Steel Fence'),
        Text.yellow('2x Fluid Pipe'),
        Text.yellow('4x Wooden Barrel')
    ])
    event.add('immersiveengineering:arc_furnace', [
        Text.yellow('08x Steel Sheetmetal'),
        Text.yellow('06x Block of Steel'),
        Text.yellow('14x Steel Sheetmetal Slab'),
        Text.yellow('05x Steel Scaffolding'),
        Text.yellow('05x Heavy Engineering Block'),
        Text.yellow('01x Cauldron'),
        Text.yellow('10x Light Engineering Block'),
        Text.yellow('27x Reinforced Blast Brick')
    ])
    event.add('immersiveengineering:silo', [
        Text.yellow('04x Treated Wood Fence'),
        Text.yellow('50x Iron Sheetmetal')
    ])
    event.add('immersiveengineering:crusher', [
        Text.yellow('10x Steel Scaffolding'),
        Text.yellow('10x Light Engineering Block'),
        Text.yellow('01x Redstone Engineering Block'),
        Text.yellow('08x Steel Fence'),
        Text.yellow('09x Hopper')
    ])
    event.add('immersiveengineering:fermenter', [
        Text.yellow('6x Steel Scaffolding'),
        Text.yellow('2x Light Engineering Block'),
        Text.yellow('4x Cauldron'),
        Text.yellow('1x Redstone Engineering Block'),
        Text.yellow('4x Iron Sheetmetal'),
        Text.yellow('2x Fluid Pipe')
    ])
    event.add('immersiveengineering:mixer', [
        Text.yellow('5x Steel Scaffolding'),
        Text.yellow('4x Light Engineering Block'),
        Text.yellow('4x Iron Sheetmetal'),
        Text.yellow('1x Redstone Engineering Block'),
        Text.yellow('1x Steel Fence'),
        Text.yellow('3x Fluid Pipe')
    ])
    event.add('immersiveengineering:tank', [
        Text.yellow('04x Treated Wood Fence'),
        Text.yellow('34x Iron Sheetmetal')
    ])
    event.add('immersiveengineering:sawmill', [
        Text.yellow('8x Steel Scaffolding'),
        Text.yellow('6x Light Engineering Block'),
        Text.yellow('4x Iron Sheetmetal'),
        Text.yellow('2x Heavy Engineering Block'),
        Text.yellow('1x Redstone Engineering Block'),
        Text.yellow('4x Conveyor Belt')
    ])
    event.add('immersiveengineering:refinery', [
        Text.yellow('08x Steel Scaffolding'),
        Text.yellow('02x Light Engineering Block'),
        Text.yellow('02x Heavy Engineering Block'),
        Text.yellow('16x Iron Sheetmetal'),
        Text.yellow('01x Redstone Engineering Block'),
        Text.yellow('05x Fluid Pipe')
    ])
    event.add('immersiveengineering:bottling_machine', [
        Text.yellow('2x Iron Sheetmetal'),
        Text.yellow('3x Steel Scaffolding'),
        Text.yellow('1x Redstone Engineering Block'),
        Text.yellow('2x Light Engineering Block'),
        Text.yellow('3x Conveyor Belt'),
        Text.yellow('Fluid Pump')
    ])
    event.add('immersiveengineering:lightning_rod', [
        Text.yellow('4x Steel Scaffolding'),
        Text.yellow('3x High-Voltage Coil Block'),
        Text.yellow('4x Treated Wood Fence'),
        Text.yellow('4x Light Engineering Block'),
        Text.yellow('8x Copper Coil Block'),
        Text.yellow('4x HV Capacitor')
    ])
    event.add('immersiveengineering:diesel_generator', [
        Text.yellow('09x Radiator Block'),
        Text.yellow('06x Steel Scaffolding'),
        Text.yellow('04x Generator Block'),
        Text.yellow('13x Heavy Engineering Block'),
        Text.yellow('01x Redstone Engineering Block'),
        Text.yellow('05x Fluid Pipe')
    ])
    event.add('immersiveengineering:excavator', [
        Text.yellow('06x Steel Scaffolding'),
        Text.yellow('16x Steel Sheetmetal'),
        Text.yellow('03x Radiator Block'),
        Text.yellow('09x Light Engineering Block'),
        Text.yellow('01x Redstone Engineering Block'),
        Text.yellow('04x Heavy Engineering Block'),
        Text.yellow('09x Block of Steel (for wheel)'),
        Text.yellow('20x Steel Scaffolding (for wheel)')
    ])
    event.add('immersiveengineering:auto_workbench', [
        Text.yellow('5x Steel Scaffolding'),
        Text.yellow('4x Light Engineering Block'),
        Text.yellow('1x Redstone Engineering Block'),
        Text.yellow('2x Heavy Engineering Block'),
        Text.yellow('2x Treated Wood Slab'),
        Text.yellow('4x Conveyor Belt')
    ])
    event.add('immersivepetroleum:pumpjack', [
        Text.yellow('11x Steel Scaffolding'),
        Text.yellow('02x Light Engineering Block'),
        Text.yellow('06x Treated Wood Fence'),
        Text.yellow('01x Redstone Engineering Block'),
        Text.yellow('02x Heavy engineering Block'),
        Text.yellow('02x Block of Steel'),
        Text.yellow('04x Steel Sheetmetal'),
        Text.yellow('04x Fluid Pipe')
    ])
    event.add('immersivepetroleum:distillationtower', [
        Text.yellow('25x Steel Scaffolding'),
        Text.yellow('04x Heavy Engineering Block'),
        Text.yellow('01x Redstone Engineering Block'),
        Text.yellow('60x Iron Sheetmetal'),
        Text.yellow('30x Steel Scaffolding Slab'),
        Text.yellow('17x Fluid Pipe')
    ])

    // WITHER-IMMUNE
    event.add([
        'engineersdecor:rebar_concrete_tile_stairs',
        'engineersdecor:rebar_concrete_tile_slab',
        'engineersdecor:rebar_concrete_tile',
        'engineersdecor:rebar_concrete_wall',
        'engineersdecor:rebar_concrete_stairs',
        'engineersdecor:rebar_concrete_slab',
        'engineersdecor:rebar_concrete',
        'thermal:enderium_glass',
        'thermal:lumium_glass',
        'thermal:signalum_glass',
        'thermal:obsidian_glass',
        'engineersdecor:panzerglass_block'
    ], [
        Text.yellow('Immune to the Wither!'),
    ])
})
